
resource "aws_s3_bucket" "stateful-bucket-test" {
  count = length(var.bucket_name)
  bucket = var.bucket_name[count.index]
}


resource "aws_s3_bucket_acl" "stateful-bucket-test" {
  count = length(var.bucket_name)
  bucket = aws_s3_bucket.stateful-bucket-test[count.index].id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "versioning_stateful-bucket-test" {
  count = length(var.bucket_name)
  bucket = aws_s3_bucket.stateful-bucket-test[count.index].id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform-state-lock"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
  
}