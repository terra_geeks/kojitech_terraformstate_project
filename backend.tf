
terraform {

  backend "s3" {
  bucket = "stateful-bucket-test"
  dynamodb_table = "terraform-state-lock"
  region = "us-east-1"
  key = "path/env"
  encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
  }
}
}